%Particle motion visualization for TPMC3D
%Directries are written in Linux format
%Input data file is trajectory.dat

%Preambles
close all
clear all
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',14)
set(0,'defaultAxesFontSize',14);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 6 4]);

%-----Parameters (Change here)-----
%Directories for input data and output figures
dirin  = '../output/datafile/';
dirout = '../figure/datafile/';
%Input and output file name
fname  = 'trajectory_neu';
%Grid parameter
XL   = 40;
YL   = 200;
ZL   = 10;
XANO = 20;
XSEP = 15;
SeparaterON = 1;
AnodeON     = 0;
%Particle number to display motion (1-5)
pnum = 1;
%----------------------------------

%Download particle position data
data=dlmread([strcat(dirin,fname),'.dat']);
leng = length(data);
%leng = length(data(:,1));
x = data(1:leng, 3*pnum-2);
y = data(1:leng, 3*pnum-1);
z = data(1:leng, 3*pnum  );

xbox1 = [0 0 1 1]*XANO;
ybox1 = [0 1 1 0]*YL;
zbox1 = [0 0 0 0]*ZL;

xbox2 = [0 0 1 1]*XANO;
ybox2 = [0 1 1 0]*YL;
zbox2 = [1 1 1 1]*ZL;

xbox3 = [1 1 1 1]*0.01*XANO;
ybox3 = [0 1 1 0]*YL;
zbox3 = [0 0 1 1]*ZL;

leng = 100;
arr = 0:1/(leng-1):1;
xbox4 = zeros(1,leng);
ybox4 = cos(arr*1*pi)*3+YL/8;
zbox4 = sin(arr*1*pi)*3+ZL/2;
xbox5 = zeros(1,leng);
ybox5 = cos(arr*1*pi)*3+YL*3/8;
zbox5 = sin(arr*1*pi)*3+ZL/2;
xbox6 = zeros(1,leng);
ybox6 = cos(arr*1*pi)*3+YL*5/8;
zbox6 = sin(arr*1*pi)*3+ZL/2;
xbox7 = zeros(1,leng);
ybox7 = cos(arr*1*pi)*3+YL*7/8;
zbox7 = sin(arr*1*pi)*3+ZL/2;
xbox9 = [0 0 0 0];
ybox9 = [1 1 0 0]*YL;
zbox9 = [0.5 1 1 0.5]*ZL;


xbox8 = horzcat(xbox4,xbox5,xbox6,xbox7,xbox9);
ybox8 = horzcat(ybox4,ybox5,ybox6,ybox7,ybox9);
zbox8 = horzcat(zbox4,zbox5,zbox6,zbox7,zbox9);


xbox42 = zeros(1,leng);
ybox42 = cos((arr+1)*1*pi)*3+YL/8;
zbox42 = sin((arr+1)*pi)*3+ZL/2;
xbox52 = zeros(1,leng);
ybox52 = cos((arr+1)*1*pi)*3+YL*3/8;
zbox52 = sin((arr+1)*1*pi)*3+ZL/2;
xbox62 = zeros(1,leng);
ybox62 = cos((arr+1)*1*pi)*3+YL*5/8;
zbox62 = sin((arr+1)*1*pi)*3+ZL/2;
xbox72 = zeros(1,leng);
ybox72 = cos((arr+1)*1*pi)*3+YL*7/8;
zbox72 = sin((arr+1)*1*pi)*3+ZL/2;
xbox92 = [0 0 0 0];
ybox92 = [1 1 0 0]*YL;
zbox92 = [0.5 0 0 0.5]*ZL;

xbox82 = horzcat(xbox42,xbox52,xbox62,xbox72,xbox92);
ybox82 = horzcat(ybox42,ybox52,ybox62,ybox72,ybox92);
zbox82 = horzcat(zbox42,zbox52,zbox62,zbox72,zbox92);

xbox43 = zeros(1,leng);
ybox43 = cos(arr*2*pi)*3+YL/8;
zbox43 = sin(arr*2*pi)*3+ZL/2;
xbox53 = zeros(1,leng);
ybox53 = cos(arr*2*pi)*3+YL*3/8;
zbox53 = sin(arr*2*pi)*3+ZL/2;
xbox63 = zeros(1,leng);
ybox63 = cos(arr*2*pi)*3+YL*5/8;
zbox63 = sin(arr*2*pi)*3+ZL/2;
xbox73 = zeros(1,leng);
ybox73 = cos(arr*2*pi)*3+YL*7/8;
zbox73 = sin(arr*2*pi)*3+ZL/2;

xboxs = [0 1 1 0]*XSEP;
yboxs = [0 0 0 0];
zboxs = [0 0 1 1]*ZL;
yboxs1= yboxs;
yboxs2= yboxs+YL*1/4;
yboxs3= yboxs+YL*2/4;
yboxs4= yboxs+YL*3/4;
yboxs5= yboxs+YL*4/4;


%Make figure of particle trajectory
plot3(x*1e3,y*1e3,z*1e3,'bo','MarkerSize',1.5);
grid off
hold on
if SeparaterON == 1
s1 = fill3(xboxs,yboxs1,zboxs,[0.3 0.3 0.3],'LineStyle','none');
alpha(s1,0.7)
hold on
s2 = fill3(xboxs,yboxs2,zboxs,[0.3 0.3 0.3],'LineStyle','none');
alpha(s2,0.7)
hold on
s3 = fill3(xboxs,yboxs3,zboxs,[0.3 0.3 0.3],'LineStyle','none');
alpha(s3,0.7)
hold on
s4 = fill3(xboxs,yboxs4,zboxs,[0.3 0.3 0.3],'LineStyle','none');
alpha(s4,0.7)
hold on
s5 = fill3(xboxs,yboxs5,zboxs,[0.3 0.3 0.3],'LineStyle','none');
alpha(s5,0.7)
hold on
   if AnodeON == 1
   f1 = fill3(xbox1,ybox1,zbox1,[0.9 0.7 0.1],'LineStyle','none');
   alpha(f1,0.7)
   hold on
   f2 = fill3(xbox2,ybox2,zbox2,[0.9 0.7 0.1],'LineStyle','none');
   alpha(f2,0.7)
   hold on
   f4 = fill3(xbox8,ybox8,zbox8,[0.9 0.7 0.1],'LineStyle','none');
   alpha(f4,0.7)
   hold on
   f5 = fill3(xbox82,ybox82,zbox82,[0.9 0.7 0.1],'LineStyle','none');
   alpha(f5,0.7)
   hold on
   f6 = fill3(xbox43,ybox43,zbox43,[0.9 0.7 0.1],'LineWidth',1);
   alpha(f6,0.0)
   hold on
   f7 = fill3(xbox53,ybox53,zbox53,[0.9 0.7 0.1],'LineWidth',1);
   alpha(f7,0.0)
   hold on
   f8 = fill3(xbox63,ybox63,zbox63,[0.9 0.7 0.1],'LineWidth',1');
   alpha(f8,0.0)
   hold on
   f9 = fill3(xbox73,ybox73,zbox73,[0.9 0.7 0.1],'LineWidth',1);
   alpha(f9,0.0)
   hold on
   end
end
xlabel('$Z$, mm','FontSize',14,'Interpreter','Latex')
ylabel('$\theta$, mm','FontSize',14,'Interpreter','Latex')
zlabel('$R$, mm','FontSize',14,'Interpreter','Latex')
set(gca,'XLim',[0,XL]);
set(gca,'YLim',[0,YL]);
set(gca,'ZLim',[0,ZL]);
saveas(figure(1),strcat(dirout,'trajectory.png'));
hold off




