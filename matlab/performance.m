%Force and torque visualization for TPMC3D
%Directries are written in Linux format
%Input data file is performance.dat

%Preambles
clear all
close all

%-----Parameters (Change here)-----
%Directories for input data and output figures
dirin  = '/Users/kawashima/Dropbox/TPMC3D/output/datafile/';
dirout = '/Users/kawashima/Dropbox/TPMC3D/figure/datafile/';
fname = 'performance';
%Maximum value for horizontal axis
maxnstp  = 100;
dtpic    = 0.521e-6;%Time step [s]
ismp     = 100;
%Calculate average force between avemin avemax
avemin   = 90;
avemax   = 100;
%Atomospheric number density [m-3]
nnatm    = 1.0d11;
%Maximum and minimum values in vertical axis for force [N] and torque [Nm]
maxforce  = 50.00e-9;
minforce  =-10.00e-9;
maxtorque = 10.00e-9;
mintorque =-10.00e-9;
%----------------------------------

a = 1;
b = ones(1,5)/5;

%Download performance data
data = dlmread([strcat(dirin,fname),'.dat']);
nstp     = data(:,1);
xforce   = data(:,2)*nnatm;
yforce   = data(:,3)*nnatm;
zforce   = data(:,4)*nnatm;
xtorque  = data(:,5)*nnatm;
ytorque  = data(:,6)*nnatm;
ztorque  = data(:,7)*nnatm;
xforcefilt=filter(b,a,xforce);
xforceave= zeros(length(data),1);
xforceave(:,1)= mean(xforce(avemin:avemax));
yforcefilt=filter(b,a,yforce);
yforceave= zeros(length(data),1);
yforceave(:,1)= mean(yforce(avemin:avemax));
zforcefilt=filter(b,a,zforce);
zforceave= zeros(length(data),1);
zforceave(:,1)= mean(zforce(avemin:avemax));
xtorquefilt=filter(b,a,xtorque);
xtorqueave= zeros(length(data),1);
xtorqueave(:,1)= mean(xtorque(avemin:avemax));
ytorquefilt=filter(b,a,ytorque);
ytorqueave= zeros(length(data),1);
ytorqueave(:,1)= mean(ytorque(avemin:avemax));
ztorquefilt=filter(b,a,ztorque);
ztorqueave= zeros(length(data),1);
ztorqueave(:,1)= mean(ztorque(avemin:avemax));

%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,xforcefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,xforceave*1e9,'k--')
str1 = ['Average: ' strcat(num2str(floor(xforceave(1,1)*1e10)/10),' nN')];
annotation('textbox', [0.58,0.7,0.4,0.2],'String',str1,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('x-Force, nN','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [minforce, maxforce]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'forcex.png']);
hold off
close all

%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,yforcefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,yforceave*1e9,'k--')
str1 = ['Average: ' strcat(num2str(floor(yforceave(1,1)*1e10)/10),' nN')];
annotation('textbox', [0.58,0.7,0.4,0.2],'String',str1,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('y-Force, nN','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [minforce, maxforce]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'forcey.png']);
hold off
close all

%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,zforcefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,zforceave*1e9,'k--')
str1 = ['Average: ' strcat(num2str(floor(zforceave(1,1)*1e10)/10),' nN')];
annotation('textbox', [0.58,0.7,0.4,0.2],'String',str1,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('z-Force, nN','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [minforce, maxforce]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'forcez.png']);
hold off
close all


%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,xtorquefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,xtorqueave*1e9,'k--')
str2 = ['Average: ' strcat(num2str(floor(xtorqueave(1,1)*1e10)/10),' nNm')];
annotation('textbox', [0.54,0.7,0.4,0.2],'String',str2,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('x-Torque, nNm','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [mintorque, maxtorque]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'torquex.png']);
hold off
close all

%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,ytorquefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,ytorqueave*1e9,'k--')
str2 = ['Average: ' strcat(num2str(floor(ytorqueave(1,1)*1e10)/10),' nNm')];
annotation('textbox', [0.54,0.7,0.4,0.2],'String',str2,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('y-Torque, nNm','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [mintorque, maxtorque]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'torquey.png']);
hold off
close all

%Make figure of plasma drag force
set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 5.5 4.5]);
plot(nstp*dtpic*ismp*1e3,ztorquefilt*1e9,'b-')
hold on
plot(nstp*dtpic*ismp*1e3,ztorqueave*1e9,'k--')
str2 = ['Average: ' strcat(num2str(floor(ztorqueave(1,1)*1e10)/10),' nNm')];
annotation('textbox', [0.54,0.7,0.4,0.2],'String',str2,'FontSize',16,'LineStyle','none')
xlabel('Time, ms','FontSize',16)
ylabel('z-Torque, nNm','FontSize',16)
set(gca, 'XLim', [0, maxnstp*dtpic*ismp*1e3]);
set(gca, 'YLim', [mintorque, maxtorque]*1e9)
%set(gca,'XTick',[0.0 2.0 4.0 6.0 8.0 10.0 12.0])
%set(gca,'XTickLabel',{'0.0','2.0','4.0','6.0','8.0','10.0','12.0'})
%set(gca,'YTick',[0 5 10 15 20 25])
%set(gca,'YTickLabel',{'0.0','5.0','10.0','15.0','20.0','25.0'})
saveas(figure(1),[dirout,'torquez.png']);
hold off
close all







