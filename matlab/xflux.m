%Flux distribution visualization for HNP3D calculation
%Directries are written in Linux format
%Input data file is xflux.******.dat

%Preambles
clear all
close all

%-----Parameters (Change here)-----
%Directories for input data and output figures
dirin  = '../output/flux/';
dirout = '../figure/distribution/';
fname1 = 'xflux';
%Average the data of fileini ~ fileend
fileini = 150;
fileend = 250;
%Grid parameter
ny    = 200;
nz    = 10;
YL    = 200;  %[mm] y-Field length
ZL    = 10;   %[mm] Anode width
XOUTPUT = 20;   %[mm] x-position of flux output

xflux_max = 2.0e22;
xflux_min = 0;
%----------------------------------

dy = YL/(ny-1);
dz = ZL/(nz-1);
yy = 0:dy:YL;
zz = 0:dz:ZL;
%Preparation of arrays
xflux2D = zeros(nz,ny);
xflux1D = zeros(ny,1);

%Average the data of fileini ~ fileend
for ii=fileini:1:fileend
   if ii<10
      fname2  = '.00000';
   elseif ii<100
      fname2  = '.0000';
   elseif ii<1000
      fname2  = '.000';
   elseif ii<10000
      fname2  = '.00';
   else
      fname2  = '.0';
   end
   fname = strcat(dirin,fname1,fname2,num2str(ii));
   data  = dlmread([fname,'.dat']);

    for k = 1:1:nz
        for j = 1:1:ny
            xflux2D(k,j) = xflux2D(k,j)+data(ny*(k-1)+j,1)/(fileend-fileini+1);
        end
    end
end


for j = 1:1:ny
    xflux1D(j) = mean(xflux2D(:,j));
end

set(0,'defaultAxesFontName', 'Times');
set(0,'defaultTextFontName', 'Times');
set(0,'defaultUicontrolFontSize',16);
set(0,'defaultAxesFontSize',16);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [2 1 10 4]);

%xflux2D Plot
   [c,h]=contourf(yy,zz,xflux2D/1e22,100);
   set(h,'edgecolor','none')
   shading interp
   grid off
   h1 = colorbar;
   ylabel(h1,'Neutral flux density, $\times10^{22}$ m$^{-2}$s$^{-1}$','FontSize',16,'Interpreter','Latex')
   caxis([xflux_min xflux_max]/1e22)
   colormap('jet')
   xlabel('Azimuthal position, mm','FontSize',16,'Interpreter','Latex')
   ylabel('Radial position, mm','FontSize'     ,16,'Interpreter','Latex')
   set(gca,'XLim',[0,YL]);
   set(gca,'YLim',[0,ZL]);

   saveas(figure(1),strcat(dirout,'xflux2D.png'));
   hold off

%xflux1D Plot
   set(gcf, 'PaperPosition', [2 1 5 4]);
   plot(yy,xflux1D/1e22,'ko-','MarkerSize',7,'LineWidth',1.0)
   xlabel('Azimuthal position, mm','FontSize',16,'Interpreter','Latex')
   ylabel('Neutral flux density, $\times10^{22}$ m$^{-2}$s$^{-1}$','FontSize',16,'Interpreter','Latex')
   set(gca, 'XLim', [0,YL]);
   set(gca, 'YLim', [xflux_min,xflux_max]/1e22)
   str1 = ['Z = ' strcat(num2str(XOUTPUT),' mm')];
   annotation('textbox', [0.68,0.7,0.4,0.2],'String',str1,'FontSize',16,'LineStyle','none')
   %set(gca,'XTick',[0 15 30 45 60 75 90])
   %set(gca,'XTickLabel',{'0','1','2','3','4','5','6'});
   %set(gca,'YTick',[0 20 40 60 80 100 120])
   %set(gca,'YTickLabel',{'0','20','40','60','80','100','120'})
   saveas(figure(1),strcat(dirout,'xflux1D.png'));
   hold off

