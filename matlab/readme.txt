更新日： 01/02/2018 by R. Kawashima
HNP3Dコードmatlabマニュアル

＊注：matlabファイルは全てLinux/Mac流のパス指定になっています！
Windowsで使うときはパスの書き方を修正すること。

distribution.m
数密度の3次元分布を可視化することができます。
平均化されたデータを用いるが、さらにfileini〜fileend
の番号のファイルを平均化して表示します。
SeparaterONを1にすると仕切り板を表示し、
AnodeONを1にするとホローアノードを表示します。
コンターのレンジは適宜変更してみて下さい。

xflux.m
x方向数流束を可視化することができます。
Y-Z 2次元分布とY 1次元分布を表示します。
（Fig中ではYはtheta, ZはRとなっています）
平均化されたデータを用いるが、さらにfileini〜fileend
の番号のファイルを平均化して表示します。
XOUTPUTの値はparameter.f90の値とそろえて下さい。

motion.m
各粒子の軌道を表示します。pnumを適宜変更する。

operation.m
各境界に流れる流束の時間履歴。計算が収束したかどうか、
粒子数が保存しているかの確認に用いる。

