rm -r output
mkdir output
cd output
mkdir datafile distribution flux
cd ..
rm nohup.out
rm -r ifortfile
mkdir ifortfile
ifort -O3 -openmp -openmp-report2 -ipo -parallel -traceback -CB -gen_interfaces -warn all -o HNP3D.exe \
program/parameter.f90 \
program/global.f90 \
program/mersenne.f90 \
program/initialcondition.f90 \
program/neutralmotion.f90 \
program/picmisc.f90 \
program/main.f90
mv *.mod ifortfile/
mv *__genmod.f90 ifortfile/

