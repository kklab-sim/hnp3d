
! ********** Euler method for free movement*****************************

subroutine EULERFREE_3DCART(nm,pic,post)

   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer              :: m
   integer, intent(in)  :: nm
   type(particle),dimension(1:NMAX),intent(in)  :: pic
   type(particle),dimension(1:NMAX),intent(out) :: post

   !!!$omp parallel default(none),shared(nm,pic,post),private(m)
   !!!$omp do
   do m = 1,nm
      post(m)%xx = pic(m)%xx+DTPIC*pic(m)%vx
      post(m)%yy = pic(m)%yy+DTPIC*pic(m)%vy
      post(m)%zz = pic(m)%zz+DTPIC*pic(m)%vz
      post(m)%vx = pic(m)%vx
      post(m)%vy = pic(m)%vy
      post(m)%vz = pic(m)%vz
   enddo
   !!!$omp end do
   !!!$omp end parallel

   return
endsubroutine


!********** Treatment of particles on boundary *************************

subroutine DELETEPARTICLE(nm,neu,ndel,lcrd,search)

   use parameters_mod
   use global_mod
   implicit none
   integer,intent(inout)   :: nm
   integer,intent(in)      :: ndel
   integer                 :: m,k
   integer,dimension(NMAX) :: lcrd,search
   type(particle),dimension(1:NMAX),intent(inout)  :: neu     !Neutral particle information

   !Delete  particles
   do m = 1,ndel
      k = lcrd(m)
      if(k.lt.1 .or. k.gt.nm) then
         write(*,*) 'Error in deleting...',k,nm,ndel,m
         cycle
      endif
      if(search(nm) .gt. 0) then
         lcrd(search(nm)) = k
         search(k) = search(nm)
      endif
      neu(k) = neu(nm)
      nm     = nm-1
   enddo

   return
endsubroutine


!********** Tracking trajectories of particles *************************

subroutine TRAJECTORY(pic,pname)

   use parameters_mod
   use global_mod
   implicit none
   type(particle),dimension(1:NMAX),intent(in)  :: pic     !Neutral particle information
   character(len=3)                ,intent(in)  :: pname

   open(unit=43,file='./output/datafile/trajectory_'//trim(pname)//'.dat', &
      form='formatted',status='unknown',position='append')
         write(43,'(15E15.5)') &
            pic(int(dble(NMINI)*0.10d0))%xx,pic(int(dble(NMINI)*0.10d0))%yy,pic(int(dble(NMINI)*0.10d0))%zz, &
            pic(int(dble(NMINI)*0.20d0))%xx,pic(int(dble(NMINI)*0.20d0))%yy,pic(int(dble(NMINI)*0.20d0))%zz, &
            pic(int(dble(NMINI)*0.30d0))%xx,pic(int(dble(NMINI)*0.30d0))%yy,pic(int(dble(NMINI)*0.30d0))%zz, &
            pic(int(dble(NMINI)*0.40d0))%xx,pic(int(dble(NMINI)*0.40d0))%yy,pic(int(dble(NMINI)*0.40d0))%zz, &
            pic(int(dble(NMINI)*0.50d0))%xx,pic(int(dble(NMINI)*0.50d0))%yy,pic(int(dble(NMINI)*0.50d0))%zz
   close(43)

   return
endsubroutine


!********** Density calculator of particles ****************************

subroutine DENSITY(nm,neu,ndcl,pname)
   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer                                   ,intent(in)    :: nm
   type(particle)  ,dimension(1:NMAX)        ,intent(inout) :: neu     !Neutral particle information
   double precision,dimension(1:NX,1:NY,1:NZ),intent(out)   :: ndcl
   character(len=3)                          ,intent(in)    :: pname
   double precision,dimension(1:NX,1:NY,1:NZ)               :: nacl
   double precision :: xc,yc,zc,xx,yy,zz,np
   double precision,dimension(8) :: p
   integer :: i,j,k,m,ip,jp,kp,ip1,ip2,jp1,jp2,kp1,kp2


   !$omp parallel default(none),shared(nm,neu,nacl,pname),private(m,ip,jp,kp,ip1,ip2,jp1,jp2,kp1,kp2,xc,yc,zc,xx,yy,zz,np,p)
   !$omp do
   do m = 1,nm
      xx = neu(m)%xx
      yy = neu(m)%yy
      zz = neu(m)%zz
      np = neu(m)%np
      ip = int(xx/DXL+0.5d0)
      jp = int(yy/DYL+0.5d0)
      kp = int(zz/DZL+0.5d0)
      if(np.lt.0.0d0) then
         write(*,*) 'Warning...Negative particle ',pname,m,ip,jp,kp,xx,yy,zz
      endif
      if(ip.le.-1 .or. ip.ge.NX+1 .or. &
         jp.le.-1 .or. jp.ge.NY+1 .or. &
         kp.le.-1 .or. kp.ge.NZ+1) then
         write(*,*) 'Warning...Irregular particle position...density',pname,m,ip,jp,kp,xx,yy,zz
      else
         xc   = (xx-DXL*(dble(ip)-0.5d0))/DXL
         yc   = (yy-DYL*(dble(jp)-0.5d0))/DYL
         zc   = (zz-DZL*(dble(kp)-0.5d0))/DZL
         if(xc.lt.0.0d0 .or. xc.gt.1.0d0) write(*,*) 'Error in Density...xc',pname,m,xc,xx,ip
         if(yc.lt.0.0d0 .or. yc.gt.1.0d0) write(*,*) 'Error in Density...yc',pname,m,yc,yy,jp
         if(zc.lt.0.0d0 .or. zc.gt.1.0d0) write(*,*) 'Error in Density...zc',pname,m,zc,zz,kp
         p(1) = (1.0d0-xc)*(1.0d0-yc)*(1.0d0-zc)
         p(2) = xc        *(1.0d0-yc)*(1.0d0-zc)
         p(3) = (1.0d0-xc)*yc        *(1.0d0-zc)
         p(4) = xc        *yc        *(1.0d0-zc)
         p(5) = (1.0d0-xc)*(1.0d0-yc)*zc
         p(6) = xc        *(1.0d0-yc)*zc
         p(7) = (1.0d0-xc)*yc        *zc
         p(8) = xc        *yc        *zc
         !--------------------------------------------------------
         if(ip.eq.0 ) then; ip1 = ip+1; else; ip1 = ip  ; endif
         if(ip.eq.NX) then; ip2 = ip  ; else; ip2 = ip+1; endif
         if(jp.eq.0 ) then; jp1 = jp+1; else; jp1 = jp  ; endif
         if(jp.eq.NY) then; jp2 = jp  ; else; jp2 = jp+1; endif
         if(kp.eq.0 ) then; kp1 = kp+1; else; kp1 = kp  ; endif
         if(kp.eq.NZ) then; kp2 = kp  ; else; kp2 = kp+1; endif
         nacl(ip1,jp1,kp1) = nacl(ip1,jp1,kp1)+p(1)*np
         nacl(ip2,jp1,kp1) = nacl(ip2,jp1,kp1)+p(2)*np
         nacl(ip1,jp2,kp1) = nacl(ip1,jp2,kp1)+p(3)*np
         nacl(ip2,jp2,kp1) = nacl(ip2,jp2,kp1)+p(4)*np
         nacl(ip1,jp1,kp2) = nacl(ip1,jp1,kp2)+p(5)*np
         nacl(ip2,jp1,kp2) = nacl(ip2,jp1,kp2)+p(6)*np
         nacl(ip1,jp2,kp2) = nacl(ip1,jp2,kp2)+p(7)*np
         nacl(ip2,jp2,kp2) = nacl(ip2,jp2,kp2)+p(8)*np
      endif
      neu(m)%ip = ip
      neu(m)%jp = jp
      neu(m)%kp = kp
   enddo
   !$omp end do
   !$omp end parallel

   do k = 1,NZ
      do j = 1,NY
         do i = 1,NX
            ndcl(i,j,k) = nacl(i,j,k)/(DXL*DYL*DZL)
         enddo
      enddo
   enddo


   return
endsubroutine


! ********** Maxwell Distribution Generator ****************************

subroutine Maxwell(Te,mass,v1,v2,v3)

   use mtmod
   implicit none
   double precision,parameter          :: pi          = 3.141592653589793d0
   double precision,parameter          :: pi2         = pi*2.0d0
   double precision,parameter          :: eu          = 1.60217733d-19
   double precision,parameter          :: kboltz      = 1.3806504d-23   ! Boltzman constant
   double precision                    :: v1,v2,v3
   double precision                    :: Te,mass,v2t
   double precision                    :: x,y

   v2t = 2.0d0*kboltz*Te/mass
   v1  = dsqrt(-v2t*dlog(grnd()))*dcos(pi2*grnd())
   x   = dsqrt(-v2t*dlog(grnd()))
   y   = pi2*grnd()
   v2  = x*dcos(y)
   v3  = x*dsin(y)

  return
endsubroutine



