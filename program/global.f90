module global_mod

   use parameters_mod
   implicit none
   ! Time Step
   integer     :: it
   integer     :: nstp = 0
   double precision,dimension(7)    :: Jnborder

   !Averaged values
   double precision,dimension(1:NX,1:NY,NZ)    :: nn_ave
   type particle
      double precision :: xx,yy,zz,vx,vy,vz                             !3D3V position and speed
      double precision :: np                                            !Number of particles in the macroparticle
      integer          :: ip,jp,kp                                      !Cell indices in x, y, z directions
   end type
end module global_mod


