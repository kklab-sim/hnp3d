module parameters_mod

   implicit none
   !Thruster information
   double precision,parameter    :: MDOT    = 3.7d-6                    ![kg/s] Mass flow rate
   double precision,parameter    :: TI      = 850.0d0                   ![K] Neutral temperature (Anode temperature)
   double precision,parameter    :: DINLET  = 6.0d-3                    ![m] Diameter of gas inlete hole
   integer,parameter             :: NINLET  = 4                         ![-] Number of gas inlet hole
   double precision,parameter    :: RDIF    = 1.0d0                     ![-] Differential mass flow ratio
   !Program parameter
   integer,parameter             :: ITM     = 25000                     ![-] Maximum number of time steps for time marching
   integer,parameter             :: ISMP    = 100                       ![-] Sampling interval for distribution
   integer,parameter             :: PSMP    = ITM                       ![-] Sampling interval for particle infomation
   double precision,parameter    :: DTPIC   = 2.0d-7                    ![s] Time step for neutral particles
   !Grid parameter
   double precision,parameter    :: XL      = 40.0d-3                   ![m] x-Length of calculation field
   double precision,parameter    :: YL      = 200.0d-3                  ![m] y-Length of calculation field
   double precision,parameter    :: ZL      = 10.0d-3                   ![m] z-Length of calculation field
   double precision,parameter    :: XANO    = 20.0d-3                   ![m] From gas inlet to anode tip
   double precision,parameter    :: XSEP    = 15.0d-3                   ![m] From gas inlet to separater tip
   double precision,parameter    :: XOUTPUT = 20.0d-3                   ![m] x-Position to output particle flux
   integer,parameter             :: NX      = 40                        ![-] x-Number of cells
   integer,parameter             :: NY      = 200                       ![-] y-Number of cells
   integer,parameter             :: NZ      = 10                        ![-] z-Number of cells
   double precision,parameter    :: DXL     = XL/NX                     ![m] x-Length of cells
   double precision,parameter    :: DYL     = YL/NY                     ![m] y-Length of cells
   double precision,parameter    :: DZL     = ZL/NZ                     ![m] z-Length of cells
   !Particle parameter
   integer,parameter             :: NMAX    = 2000000                   !Maximum number of particles
   integer,parameter             :: NMINI   = 100000                    !Initial number of particles
   double precision,parameter    :: MACP    = 5.0d19*XL*YL*ZL/dble(NMINI) !Macroparticle size
   !Physical parameter
   double precision,parameter    :: PI      = 3.1415926535893d0
   double precision,parameter    :: ECH     = 1.60217733d-19            ![C] Elementary charge
   double precision,parameter    :: MI      = 2.196324331d-25           ![kg] Ion mass of xenon
   double precision,parameter    :: KBOLTZ  = 1.3806504d-23             ![m^2kg/s^2K] Boltzmann constant

end module parameters_mod


