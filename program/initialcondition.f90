!***********************************************************************
!*****                    initialcondition.f90                     *****
!***********************************************************************

subroutine INITIALCONDITION(nmn,neu,nn)

   use parameters_mod
   use global_mod
   use mtmod
   implicit none
   integer                                   ,intent(inout) :: nmn      ![-] Number of Neutral macropaticles
   double precision,dimension(1:NX,1:NY,1:NZ),intent(inout) :: nn       ![m-3] Neutral number densityty
   type(particle)  ,dimension(1:NMAX)        ,intent(inout) :: neu      ![-] Neutral particle information
   integer          :: m
   double precision :: aa,bb

   !Initial particle of neutrals (Uniform distribution)
   nmn = NMINI
   do m = 1,nmn
      neu(m)%xx = grnd()*XL
      neu(m)%yy = grnd()*YL
      neu(m)%zz = grnd()*ZL
      aa        = dsqrt(-2.0d0*KBOLTZ/MI*TI*dlog(grnd()))
      bb        = 2.0d0*PI*grnd()
      neu(m)%vx = aa*dcos(bb)
      neu(m)%vy = aa*dsin(bb)
      aa        = dsqrt(-2.0d0*KBOLTZ/MI*TI*dlog(grnd()))
      bb        = 2.0d0*PI*grnd()
      neu(m)%vz = aa*dcos(bb)
      neu(m)%np = MACP
   enddo

   !Calculation of initial density
   call DENSITY(nmn,neu,nn,'neu')


   return
endsubroutine






