!***********************************************************************
!***** neutralmotion.f90: Neutral PIC solver                       *****
!***** Calculate motion of neutral particles: neu                  *****
!***** Main output is neutral number density: nn                   *****
!***********************************************************************

subroutine Neu_PIC(nmn,neu,nn,nm_in,nm_del,xflux)

   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer                                   ,intent(inout)   :: nmn
   type(particle)  ,dimension(1:NMAX)        ,intent(inout)   :: neu    !Neutral particle information
   double precision,dimension(1:NX,1:NY,1:NZ),intent(inout)   :: nn
   integer                               :: m,time_pre,time_post,time_neu
   integer                               :: nm_in,nm_del
   integer,dimension(NMAX)               :: search,lcrd
   type(particle)  ,dimension(1:NMAX)    :: post
   double precision,dimension(1:NY,1:NZ) :: xflux                       !Neutral particle flux at XOUTPUT

   call system_clock(count = time_pre)

   !Incoming neutrals from anode
   call Inflow_neutral(nmn,neu,nm_in)

   !Free Movement of particles based on Euler method
   call EULERFREE_3DCART(nmn,neu,post)

   !Treatment of particles on boundary
   call Judge_neutral_diffusion(nmn,neu,post,nm_del,lcrd,search)

   call FluxCount(nmn,neu,post,xflux)

   !$omp parallel default(none),shared(nmn,neu,post),private(m)
   !$omp do
   do m = 1,nmn
      neu(m)%xx = post(m)%xx
      neu(m)%yy = post(m)%yy
      neu(m)%zz = post(m)%zz
      neu(m)%vx = post(m)%vx
      neu(m)%vy = post(m)%vy
      neu(m)%vz = post(m)%vz
   enddo
   !$omp end do
   !$omp end parallel

   call DELETEPARTICLE(nmn,neu,nm_del,lcrd,search)

   !Density calculation on cells
   call DENSITY(nmn,neu,nn,'neu')

   if(mod(it,10).eq.0) then
      call TRAJECTORY(neu,'neu')
   endif

   call system_clock(count = time_post)
   time_neu = time_post - time_pre

   return
endsubroutine


! ********** Inflow of neutral particles *******************************

subroutine Inflow_neutral(nmn,neu,nm_in)

   use mtmod
   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer                         ,intent(inout)  :: nmn
   type(particle),dimension(1:NMAX),intent(inout)  :: neu     !Neutral particle information
   double precision,dimension(1:NMAX) :: yin,zin
   integer                   :: nm_in,m
   double precision          :: aa,bb,flow,psiz,vth2

   vth2 = 2.0d0*KBOLTZ/MI*TI
   flow = MDOT/MI*DTPIC
   nm_in = int(flow/MACP)+1
   psiz = flow/dble(nm_in)
   call InflowHole(nm_in,yin,zin)
   Jnborder(1) = Jnborder(1)+flow
   ! ---------- Ion inflow from left side ------------------------------
   do m = 1,nm_in
      nmn = nmn+1
      aa = dsqrt(-vth2*dlog(grnd()))
      bb = 2.0d0*PI*grnd()
      neu(nmn)%vx  = dabs(aa*dcos(bb))
      aa = dsqrt(-vth2*dlog(grnd()))
      bb = 2.0d0*PI*grnd()
      neu(nmn)%vy  = aa*dcos(bb)
      neu(nmn)%vz  = aa*dsin(bb)
      neu(nmn)%xx  = grnd()*DTPIC*neu(nmn)%vx
      neu(nmn)%yy  = yin(m)
      neu(nmn)%zz  = zin(m)
      neu(nmn)%np  = psiz
   enddo

   return
endsubroutine

! ********** Inflow Hole shape *******************************

subroutine InflowHole(nm_in,yin,zin)

   use mtmod
   use parameters_mod
   use global_mod
   implicit none
   integer            ,intent(in)     :: nm_in
   double precision,dimension(1:NMAX) :: yin,zin
   integer                            :: m,Hole
   double precision                   :: Y1_hole,Int_hole
   double precision                   :: yc,zc,rp,theta,h


   Y1_hole  = YL/dble(NINLET)*0.5d0
   Int_hole = YL/dble(NINLET)
   do m = 1,nm_in
      h      = grnd()
      Hole   = int(h/0.5d0)*2+int((h-int(h/0.5d0)*0.5d0)/(0.25d0+RDIF*0.25d0))
      if(Hole.le.-1 .or. Hole.ge.4) write(*,*) 'Error in InflowHole'
      yc     = Y1_hole+dble(Hole)*Int_hole
      zc     = 0.5d0*ZL
      rp     = dsqrt(grnd())*DINLET*0.5d0
      theta  = 2.0d0*PI*grnd()
      yin(m) = yc+rp*dcos(theta)
      zin(m) = zc+rp*dsin(theta)
   enddo

   return
endsubroutine

!********** Treatment of particles on boundary *************************

subroutine Judge_neutral_diffusion(nmn,neu,post,nm_del,lcrd,search)

   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer,intent(inout)   :: nmn
   integer,dimension(NMAX),intent(out) :: lcrd,search
   integer :: nmb,mb
   integer,dimension(NMAX/100,24) :: m_bound
   type(particle)  ,dimension(1:NMAX)        ,intent(in)      :: neu    !Neutral particle information
   type(particle)  ,dimension(1:NMAX)        ,intent(out)     :: post
   integer,intent(out)     :: nm_del
   integer                 :: m,k
   double precision        :: dt,x,v1,v2,v3
   double precision        :: xx1,yy1,zz1,vx1,vy1,vz1
   double precision        :: xx2,yy2,zz2,vx2,vy2,vz2
   integer,dimension(8)    :: case_sep
   integer :: thnum,mnum


   ! Initialization
   nm_del = 0
   search = 0
   lcrd   = 0
   nmb = 0
   m_bound = 0

   !$omp parallel default(none),shared(nmn,neu,post,m_bound),reduction(+:nmb),private(m,xx1,yy1,zz1,xx2,yy2,zz2,case_sep)
   !$omp do
   do m = 1,nmn
      xx1 = neu(m)%xx
      yy1 = neu(m)%yy
      zz1 = neu(m)%zz
      xx2 = post(m)%xx
      yy2 = post(m)%yy
      zz2 = post(m)%zz
      case_sep = 0
      if((yy2-0.00d0*YL).lt.0.0d0 .and. (yy1-0.00d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(1) = 1
      if((yy2-0.25d0*YL).lt.0.0d0 .and. (yy1-0.25d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(2) = 1
      if((yy2-0.50d0*YL).lt.0.0d0 .and. (yy1-0.50d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(3) = 1
      if((yy2-0.75d0*YL).lt.0.0d0 .and. (yy1-0.75d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(4) = 1
      if((yy2-1.00d0*YL).gt.0.0d0 .and. (yy1-1.00d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(5) = 1
      if((yy2-0.75d0*YL).gt.0.0d0 .and. (yy1-0.75d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(6) = 1
      if((yy2-0.50d0*YL).gt.0.0d0 .and. (yy1-0.50d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(7) = 1
      if((yy2-0.25d0*YL).gt.0.0d0 .and. (yy1-0.25d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(8) = 1
      if(xx2.gt.0.0d0 .and. xx2.lt.XL .and. &
         yy2.gt.0.0d0 .and. yy2.lt.YL .and. &
         zz2.gt.0.0d0 .and. zz2.lt.ZL .and. &
         case_sep(1).eq.0 .and. case_sep(2).eq.0 .and. &
         case_sep(3).eq.0 .and. case_sep(4).eq.0 .and. &
         case_sep(5).eq.0 .and. case_sep(6).eq.0 .and. &
         case_sep(7).eq.0 .and. case_sep(8).eq.0) then
      else
         nmb = nmb+1
         m_bound(nmb,omp_get_thread_num()+1) = m
      endif
   enddo
   !$omp end do
   !$omp end parallel

   thnum = 0
   mnum  = 0
   JUD: do mb = 1,nmb
      mnum = mnum+1
      if(m_bound(mnum,thnum+1).eq.0) then
         thnum = thnum+1
         mnum  = 1
      endif
      m   = m_bound(mnum,thnum+1)
      xx1 = neu(m)%xx
      yy1 = neu(m)%yy
      zz1 = neu(m)%zz
      vx1 = neu(m)%vx
      vy1 = neu(m)%vy
      vz1 = neu(m)%vz
      xx2 = post(m)%xx
      yy2 = post(m)%yy
      zz2 = post(m)%zz
      vx2 = post(m)%vx
      vy2 = post(m)%vy
      vz2 = post(m)%vz
      dt  = DTPIC
   BCN: do k = 1,5
         case_sep = 0
         if((yy2-0.00d0*YL).lt.0.0d0 .and. (yy1-0.00d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(1) = 1
         if((yy2-0.25d0*YL).lt.0.0d0 .and. (yy1-0.25d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(2) = 1
         if((yy2-0.50d0*YL).lt.0.0d0 .and. (yy1-0.50d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(3) = 1
         if((yy2-0.75d0*YL).lt.0.0d0 .and. (yy1-0.75d0*YL).ge.0.0d0 .and. xx2.le.XSEP) case_sep(4) = 1
         if((yy2-1.00d0*YL).gt.0.0d0 .and. (yy1-1.00d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(5) = 1
         if((yy2-0.75d0*YL).gt.0.0d0 .and. (yy1-0.75d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(6) = 1
         if((yy2-0.50d0*YL).gt.0.0d0 .and. (yy1-0.50d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(7) = 1
         if((yy2-0.25d0*YL).gt.0.0d0 .and. (yy1-0.25d0*YL).le.0.0d0 .and. xx2.le.XSEP) case_sep(8) = 1
         if(xx2.gt.0.0d0 .and. xx2.le.XL .and. &
            yy2.gt.0.0d0 .and. yy2.le.YL .and. &
            zz2.gt.0.0d0 .and. zz2.le.ZL .and. &
            case_sep(1).eq.0 .and. case_sep(2).eq.0 .and. &
            case_sep(3).eq.0 .and. case_sep(4).eq.0 .and. &
            case_sep(5).eq.0 .and. case_sep(6).eq.0 .and. &
            case_sep(7).eq.0 .and. case_sep(8).eq.0) then
            exit BCN
         ! In the case of x >= XL
         elseif(xx2.ge.XL) then
            Jnborder(4) = Jnborder(4)-neu(m)%np
            nm_del       = nm_del+1
            lcrd(nm_del) = m
            search(m)  = nm_del
            cycle JUD
         elseif(zz2.le.0.0d0 .and. xx2.gt.XANO) then
            Jnborder(3) = Jnborder(3)-neu(m)%np
            nm_del       = nm_del+1
            lcrd(nm_del) = m
            search(m)  = nm_del
            cycle JUD
         elseif(zz2.ge.ZL .and. xx2.gt.XANO) then
            Jnborder(5) = Jnborder(5)-neu(m)%np
            nm_del       = nm_del+1
            lcrd(nm_del) = m
            search(m)  = nm_del
            cycle JUD
         ! In the case of x <= 0
         else if(xx2.le.0.0d0) then
            x = min(1.0d0,(xx1)/(xx1-xx2))
            if(x.lt.0.0d0 .or. x.gt.1.0d0) then
               write(*,*) 'Error in neu judge at x <= 0...'
            endif
            dt  = (1.0d0-x)*dt
            xx2 = 0.0d0
            yy2 = yy1+x*(yy2-yy1)
            zz2 = zz1+x*(zz2-zz1)
            call Maxwell(TI,MI,v1,v2,v3)
            vx2 = dabs(v1)
            vy2 = v2
            if(zz2.lt.DZL) then
               vz2 = dabs(v3)
            elseif(ZL-zz2.lt.DZL) then
               vz2 =-dabs(v3)
            else
               vz2 = v3
            endif
            xx2 = xx2+vx2*dt
            yy2 = yy2+vy2*dt
            zz2 = zz2+vz2*dt
         !z <= 0
         else if(zz2.le.0.0d0) then
            x = (zz1)/(zz1-zz2)
            if(x.lt.0.0d0 .or. x.gt.1.0d0) then
               write(*,*) 'Error in neu judge at z <= 0...'
            endif
            dt  = (1.0d0-x)*dt
            zz2 = 0.0d0
            xx2 = xx1+x*(xx2-xx1)
            yy2 = yy1+x*(yy2-yy1)
            call Maxwell(TI,MI,v1,v2,v3)
            vx2 = v1
            vy2 = v2
            vz2 = dabs(v3)
            !vz2 = dabs(vz2)
            if(xx2.lt.DXL) then
               vx2 = dabs(vx2)
            endif
            xx2 = xx2+vx2*dt
            yy2 = yy2+vy2*dt
            zz2 = zz2+vz2*dt
         !z >= ZL
         else if(zz2.ge.ZL) then
            x = (ZL-zz1)/(zz2-zz1)
            if(x.lt.0.0d0 .or. x.gt.1.0d0) then
               write(*,*) 'Error in neu judge at z >= ZL...'
            endif
            dt  = (1.0d0-x)*dt
            zz2 = ZL
            xx2 = xx1+x*(xx2-xx1)
            yy2 = yy1+x*(yy2-yy1)
            call Maxwell(TI,MI,v1,v2,v3)
            vx2 = v1
            vy2 = v2
            vz2 =-dabs(v3)
            !vz2 =-dabs(vz2)
            if(xx2.lt.DXL) then
               vx2 = dabs(vx2)
            endif
            xx2 = xx2+vx2*dt
            yy2 = yy2+vy2*dt
            zz2 = zz2+vz2*dt
         !y <= 0
         else if(case_sep(1).eq.1) then
            !write(*,*) '1'
            call SEP_Plus(0.00d0*YL)
         else if(case_sep(2).eq.1) then
            !write(*,*) '2'
            call SEP_Plus(0.25d0*YL)
         else if(case_sep(3).eq.1) then
            !write(*,*) '3'
            call SEP_Plus(0.50d0*YL)
         else if(case_sep(4).eq.1) then
            !write(*,*) '4'
            call SEP_Plus(0.75d0*YL)
         else if(case_sep(5).eq.1) then
            !write(*,*) '5'
            call SEP_Minus(1.00d0*YL)
         else if(case_sep(6).eq.1) then
            !write(*,*) '6'
            call SEP_Minus(0.75d0*YL)
         else if(case_sep(7).eq.1) then
            !write(*,*) '7'
            call SEP_Minus(0.50d0*YL)
         else if(case_sep(8).eq.1) then
            !write(*,*) '8'
            call SEP_Minus(0.25d0*YL)
         !y >= YL
         else if(yy2.le.0.0d0) then
            yy2 = YL+yy2
         else if(yy2.ge.YL) then
            yy2 = 0.0d0+(yy2-YL)
         else
         endif
      enddo BCN
      if(k.eq.5) write(*,*) 'Warning: k = 5 at neutral BCN...'
      if(xx2.lt.0.0d0 .or. xx2.gt.XL .or. &
         yy2.lt.0.0d0 .or. yy2.gt.YL .or. &
         zz2.lt.0.0d0 .or. zz2.gt.ZL) then
         nm_del = nm_del+1
         lcrd(nm_del) = m
         search(m) = nm_del
         write(*,*) 'Irregular deleting in neutral...',k
         cycle JUD
      endif
      post(m)%xx = xx2
      post(m)%yy = yy2
      post(m)%zz = zz2
      post(m)%vx = vx2
      post(m)%vy = vy2
      post(m)%vz = vz2
   enddo JUD


   return
contains


   subroutine SEP_Plus(border)
      implicit none
      double precision :: border

      x = (yy1-border)/(yy1-yy2)
      if(x.lt.0.0d0 .or. x.gt.1.0d0) then
         write(*,*) 'Error in neu judge at SEP1...'
      endif
      dt  = (1.0d0-x)*dt
      yy2 = border
      xx2 = xx1+x*(xx2-xx1)
      zz2 = zz1+x*(zz2-zz1)
      call Maxwell(TI,MI,v1,v2,v3)
      if(xx2.lt.DXL) then
         vx2 = dabs(v1)
      else
         vx2 = v1
      endif
      vy2 = dabs(v2)
      if(zz2.lt.DZL) then
         vz2 = dabs(v3)
      elseif(ZL-zz2.lt.DZL) then
         vz2 =-dabs(v3)
      else
         vz2 = v3
      endif
      xx2 = xx2+vx2*dt
      yy2 = yy2+vy2*dt
      zz2 = zz2+vz2*dt

      return
   endsubroutine
   subroutine SEP_Minus(border)
      implicit none
      double precision :: border

      x = (border-yy1)/(yy2-yy1)
      if(x.lt.0.0d0 .or. x.gt.1.0d0) then
         write(*,*) 'Error in neu judge at SEP1...'
      endif
      dt  = (1.0d0-x)*dt
      yy2 = border
      xx2 = xx1+x*(xx2-xx1)
      zz2 = zz1+x*(zz2-zz1)
      call Maxwell(TI,MI,v1,v2,v3)
      if(xx2.lt.DXL) then
         vx2 = dabs(v1)
      else
         vx2 = v1
      endif
      vy2 =-dabs(v2)
      if(zz2.lt.DZL) then
         vz2 = dabs(v3)
      elseif(ZL-zz2.lt.DZL) then
         vz2 =-dabs(v3)
      else
         vz2 = v3
      endif
      xx2 = xx2+vx2*dt
      yy2 = yy2+vy2*dt
      zz2 = zz2+vz2*dt

      return
   endsubroutine

endsubroutine


!********** Treatment of particles on boundary *************************

subroutine FluxCount(nmn,neu,post,xflux)

   use parameters_mod
   use global_mod
   !$ use omp_lib
   implicit none
   integer                              ,intent(in)    :: nmn
   type(particle)  ,dimension(1:NMAX)   ,intent(in)    :: neu    !Neutral particle information
   type(particle)  ,dimension(1:NMAX)   ,intent(in)    :: post
   double precision,dimension(1:NY,1:NZ),intent(inout) :: xflux      !Neutral particle flux at XOUTPUT
   integer                 :: m,jp,kp
   double precision        :: x
   double precision        :: xx1,yy1,zz1
   double precision        :: xx2,yy2,zz2

   !$omp parallel default(none),shared(nmn,neu,post),reduction(+:xflux),private(m,xx1,yy1,zz1,xx2,yy2,zz2,x,jp,kp)
   !$omp do
   do m = 1,nmn
      xx1 = neu(m)%xx
      yy1 = neu(m)%yy
      zz1 = neu(m)%zz
      xx2 = post(m)%xx
      yy2 = post(m)%yy
      zz2 = post(m)%zz
      if((xx1-XOUTPUT).lt.0.0d0 .and. (xx2-XOUTPUT).ge.0.0d0) then
         x = min(1.0d0,(XOUTPUT-xx1)/(xx2-xx1))
         if(x.lt.0.0d0 .or. x.gt.1.0d0) then
            write(*,*) 'Error in FluxCount...'
         endif
         xx2 = XOUTPUT
         yy2 = yy1+x*(yy2-yy1)
         zz2 = zz1+x*(zz2-zz1)
         jp  = int(yy2/DYL)+1
         kp  = int(zz2/DZL)+1
         if(jp.ge.1 .and. jp.le.NY .and. kp.ge.1 .and. kp.le.NZ) then
            xflux(jp,kp) = xflux(jp,kp)+neu(m)%np
         endif
      endif
   enddo
   !$omp end do
   !$omp end parallel


   return
endsubroutine



