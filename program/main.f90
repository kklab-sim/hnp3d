!***********************************************************************
!*****     3-D Neutral Gas Flow Simulator for Hall Thrusters       *****
!*****        Developed by Rei Kawashima (Univ. of Tokyo)          *****
!*****                  Last Update 01/30/2018                     *****
!***********************************************************************

   program main
      use parameters_mod
      use global_mod
      implicit none
      integer                                       :: nmn     !Number of Neutral macropaticles
      double precision,dimension(1:NX,1:NY,1:NZ)    :: nn      ![-] Neutral number densityty
      type(particle)  ,dimension(1:NMAX)            :: neu     !Neutral particle information
      double precision,dimension(1:NY,1:NZ)         :: xflux   !Neutral particle flux at XOUTPUT
      integer :: nm_in,nm_out

      write(*,*) 'Program HNP3D start...'

      ! Initial setups
      call INITIALCONDITION(nmn,neu,nn)

      MARCH: do it = 1,ITM
         if(mod(it,ISMP).eq.0) nstp = nstp+1
         if(mod(it,ISMP).eq.1) then
            Jnborder = 0.0d0
            nn_ave   = 0.0d0
            xflux    = 0.0d0
         endif

         !Neutral PIC calculation
         call Neu_PIC(nmn,neu,nn,nm_in,nm_out,xflux)
         !Performance calculation
         call AVERAGE
         call OUTPUTDISPLAY
         call OUTPUTFILE

      enddo MARCH

      write(*,*) 'Program HNP3D end...'

      stop
   contains

!***********************************************************************

   subroutine AVERAGE
      implicit none
      integer :: i,j,k

      do k = 1,NZ
         do j = 1,NY
            do i = 1,NX
               nn_ave(i,j,k) = nn_ave(i,j,k)+nn(i,j,k)/dble(ISMP)
            enddo
         enddo
      enddo

      if(mod(it,ISMP).eq.0) then
         do k = 1,NZ
            do j = 1,NY
               xflux(j,k) = xflux(j,k)/(DTPIC*dble(ISMP))/(DYL*DZL)
            enddo
         enddo
         do k = 1,7
            Jnborder(k) = Jnborder(k)*ECH/(DTPIC*dble(ISMP))
         enddo
      endif

      return
   endsubroutine

!***********************************************************************

   subroutine OUTPUTDISPLAY
      implicit none

      integer :: m
      double precision :: vxmax,vymax

      if(mod(it,ISMP).eq.0) then

         vxmax = 0.0d0
         vymax = 0.0d0
         do m = 1,nmn
            if(dabs(neu(m)%vx).gt.vxmax) then
               vxmax = dabs(neu(m)%vx)
            endif
            if(dabs(neu(m)%vy).gt.vymax) then
               vymax = dabs(neu(m)%vy)
            endif
         enddo
         write(*,*) '***********************************************'
         write(*,'(A10,I10,A)') '          ',nstp,'-th step'
         write(*,'(A20,E12.3,A)') '    Time step =',DTPIC,' [s]'
         write(*,'(A20,E12.3,A)') '   Total time =',DTPIC*dble(nstp),' [s]'
         write(*,*) '--------------- Neutral -----------------------'
         write(*,'(A20,I10,A)')   '    N. of neu =',nmn,    ' [-]'
         write(*,'(A20,I10,A)')   ' N. of inflow =',nm_in,  ' [-]'
         write(*,'(A20,I10,A)')   'N. of outflow =',nm_out, ' [-]'
         write(*,'(A20,E12.3,E12.3,A)') '   CFL x y =',vxmax*DTPIC/DXL,vymax*DTPIC/DYL,''
         !write(*,'(A20,I10,A)')   '     CPU time in neu =',time_neu,''
         !write(*,*) '--------------- Neutral [Aeq] -----------------'
         write(*,'(A20,E12.3,A)') '       Inflow =',MDOT/MI*ECH,' [Aeq]'
         write(*,'(A20,E12.3,A)') '      Outflow =',-(Jnborder(3)+Jnborder(4)+Jnborder(5)),' [Aeq]'
         ! 1: Anode left side, 2: Anode bottom side,
         ! 3: Space bottom side,4: Space right side, 5: Space top side,
         ! 6: Anode top side, 7: Generated neutrals (always 0)
         write(*,'(A12,E12.3,E12.3)') '',Jnborder(6),Jnborder(5)
         write(*,'(E12.3,A6,E12.3,A6,E12.3)') Jnborder(1),'',Jnborder(7),'',Jnborder(4)
         write(*,'(A12,E12.3,E12.3)') '',Jnborder(2),Jnborder(3)
      endif


      return
   endsubroutine

!***********************************************************************

   subroutine OUTPUTFILE
      implicit none
      character(len=6) :: cname
      integer :: i,j,k,m

      if(mod(it,ISMP).eq.0) then
         write(cname,'(I6.6)') nstp
         open(unit=22,file='./output/distribution/distribution.'//&
            cname//'.dat',form='formatted',status='replace')
            do k = 1,NZ
               do j = 1,NY
                  do i = 1,NX
                     write(22,'(1E15.5)') &
                        nn_ave(i,j,k)
                  enddo
               enddo
            enddo
         close(22)

         open(unit=44,file='./output/datafile/operation.dat', &
            form='formatted',status='unknown',position='append')
               write(44,'(1I5,5E15.5)') &
                  nstp,MDOT/MI*ECH,Jnborder(3)+Jnborder(4)+Jnborder(5),&
                  Jnborder(3),Jnborder(4),Jnborder(5)
         close(44)

         open(unit=23,file='./output/flux/xflux.'//&
            cname//'.dat',form='formatted',status='replace')
            do k = 1,NZ
               do j = 1,NY
                  write(23,'(1E15.5)') xflux(j,k)
               enddo
            enddo
         close(23)
      endif

      if(mod(it,PSMP).eq.0) then
         write(cname,'(I6.6)') nstp
         open(unit=23,file='./output/datafile/neutralpic.'//&
            cname//'.dat',form='formatted',status='replace')
               do m = 1,nmn
                  write(23,'(7E15.5)') &
                     neu(m)%xx,neu(m)%yy,neu(m)%zz,neu(m)%vx,neu(m)%vy,neu(m)%vz,neu(m)%np
               enddo
         close(23)
      endif

      return
   endsubroutine


end program main


